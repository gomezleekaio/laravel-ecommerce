INSERT INTO payments(name) VALUES ('COD'), ('paypal'), ('stripe');

INSERT INTO statuses(name) VALUES ('pending'), ('paid'), ('cancelled');


INSERT INTO roles(name) VALUES ('admin'), ('user');

INSERT INTO categories(name) VALUES ('desktops'), ('laptops'), ('mobile devices');