<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'ItemController@index');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/catalog', 'ItemController@index');

Route::get('/cart', 'ItemController@showCart');
Route::post('/add-to-cart/{id}', 'ItemController@addToCart');
Route::delete('/cart/changeqty/{id}/{quantity}', 'ItemController@changeQty');
Route::get('/cart/empty-cart', 'ItemController@emptyCart');
Route::patch('/cart/update-qty', 'ItemController@updateQty');


Route::middleware('user')->group(function(){
	Route::get('/cart/checkout', 'OrderController@checkout');
	Route::get('/orders', 'OrderController@order');
});


Route::middleware('admin')->group(function(){
	Route::get('/admin/add-item', 'ItemController@create');
	Route::post('/admin/add-item', 'ItemController@store');
	Route::get('/admin/edit-item/{id}', 'ItemController@edit');
	Route::patch('/admin/edit-item/{id}', 'ItemController@update');

	Route::delete('/admin/delete-item', 'ItemController@destroy');
	Route::get('/admin/users', 'UserController@index');
	Route::patch('/admin/update-role/{id}', 'UserController@updateRole');
	Route::get('/admin/allorders', 'OrderController@index');
	Route::patch('/admin/cancel-order/{id}', 'OrderController@cancelOrder');
	Route::patch('/admin/mark-as-paid/{id}', 'OrderController@markAsPaid');
});