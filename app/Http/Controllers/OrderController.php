<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use App\Item;
use Auth;
use Session;

class OrderController extends Controller
{
    public function checkout()
    {
    	if (Auth::user()) {
    		$total = 0;
    		$order = new Order;
	    	$order->status_id = 1;
	    	$order->payment_id = 1;
	    	$order->user_id = Auth::user()->id;
	    	$order->total = 0;
	    	$order->save();

	    	foreach (Session::get('cart') as $id => $quantity) {
	    		$order->items()->attach($id, ["quantity" => $quantity]);
	    		$item = Item::find($id);
	    		$total += $item->price * $quantity;
	    	}

	    	$order->total = $total;
	    	$order->save();

	    	Session::forget('cart');
	    	return redirect('/catalog');
    	} else {
    		return redirect('/login');
    	}
    }

    public function order()
    {
    	$orders = Order::where('user_id', Auth::user()->id)->get();
    	return view('orders', compact('orders'));
    }

    public function index()
    {
    	$orders = Order::all();
    	return view('all-orders', compact('orders'));
    }

    public function cancelOrder($id)
    {
    	$order = Order::find($id);
    	$order->status_id = 3;
    	$order->save();
    	return redirect('/admin/allorders');
    }

    public function markAsPaid($id)
    {
    	$order = Order::find($id);
    	$order->status_id = 2;
    	$order->save();
    	return redirect('/admin/allorders');
    }
}
